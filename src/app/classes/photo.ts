interface PhotoInit {
  id: number;
  name: string;
  description: string;
  src: string;
}
export class Photo {
  constructor(private _data: PhotoInit) {
  }

  get id() { return this._data.id; }
  get name() { return this._data.name; }
  get description() { return this._data.description; }
  get src() { return this._data.src }
}
