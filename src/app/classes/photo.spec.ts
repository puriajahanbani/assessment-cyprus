import { Photo } from './photo';

describe('Photo', () => {
  it('should create an instance with data', () => {
    const photo = new Photo({ id: 1, name: 'test', description: 'test', src: 'https://picsum.photos/300/300' });

    expect(photo).toBeTruthy();
    expect(photo.id).toBe(1);
    expect(photo.name).toBe('test');
    expect(photo.description).toBe('test');
    expect(photo.src).toBe('https://picsum.photos/300/300');
  });
});
