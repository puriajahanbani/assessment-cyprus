import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Photo } from 'src/app/classes/photo';

@Component({
  selector: 'app-image-card',
  templateUrl: './image-card.component.html',
  styleUrls: ['./image-card.component.scss']
})
export class ImageCardComponent implements OnInit {

  @Input() photo!: Photo;
  @Input() isGrid = true;
  @Output() itemClicked = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
