import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { By } from '@angular/platform-browser';
import { Photo } from 'src/app/classes/photo';

import { ImageCardComponent } from './image-card.component';

describe('ImageCardComponent', () => {
  let component: ImageCardComponent;
  let fixture: ComponentFixture<ImageCardComponent>;
  let compiled: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ImageCardComponent], imports: [MatCardModule, MatRippleModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement as HTMLElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('photo', () => {
    it('should be undefined first', () => {
      expect(component.photo).toBeUndefined();
    });

    it('should render mat-card when photo is ready', () => {
      component.photo = new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' });
      fixture.detectChanges();
      const card = compiled.querySelector('.example-card');

      expect(card).toBeTruthy();
    });

    it('should emit itemClicked when click on card', () => {
      spyOn(component['itemClicked'], 'emit');
      component.photo = new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' });
      fixture.detectChanges();
      const card = fixture.debugElement.nativeElement.querySelector('.example-card');
      card.click();

      expect(component.itemClicked.emit).toHaveBeenCalledTimes(1);
    });

    it('should render name when photo is ready', () => {
      component.photo = new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' });
      fixture.detectChanges();
      const card = fixture.debugElement.query(By.css('.mat-card-title'));

      expect(card).toBeTruthy();
      expect(card.nativeElement.textContent).toContain('test');
    });

    it('should render img when photo is ready', () => {
      component.photo = new Photo({ id: 1, name: 'test', description: 'test', src: 'http://testUrl.com/' });
      fixture.detectChanges();
      const card = fixture.debugElement.nativeElement.querySelector('img');

      expect(card).toBeTruthy();
      expect(card.src).toContain('http://testurl.com/');
      expect(card.alt).toBe('test');
    });

    it('should render description when photo is ready', () => {
      component.photo = new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' });
      fixture.detectChanges();
      const card = fixture.debugElement.query(By.css('.mat-card-content'));

      expect(card).toBeTruthy();
      expect(card.nativeElement.textContent).toContain('test');
    });
  })
});
