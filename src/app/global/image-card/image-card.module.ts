import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageCardComponent } from './image-card.component';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';

@NgModule({
  declarations: [
    ImageCardComponent
  ],
  imports: [
    CommonModule, MatCardModule, MatRippleModule
  ],
  exports: [
    ImageCardComponent
  ]
})
export class ImageCardModule { }
