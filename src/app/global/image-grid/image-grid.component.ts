import { Component, HostListener, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Photo } from 'src/app/classes/photo';

@Component({
  selector: 'app-image-grid',
  templateUrl: './image-grid.component.html',
  styleUrls: ['./image-grid.component.scss']
})
export class ImageGridComponent implements OnInit {
  @Input() photos: Photo[] | null = [];
  @Output() itemClicked = new EventEmitter<Photo>();

  col = 3;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.calculateGrid()
  }
  constructor() { }

  ngOnInit(): void {
    this.calculateGrid()
  }

  calculateGrid() {
    const innerWidth = window.innerWidth;
    if (innerWidth <= 500) {
      this.col = 1;
    }

    if (innerWidth <= 800 && innerWidth > 500) {
      this.col = 2;
    }

    if (innerWidth > 800) {
      this.col = 3;
    }
  }

  identify(index: number, photo: Photo) {
    return photo.id;
  }
}
