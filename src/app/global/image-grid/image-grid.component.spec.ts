import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatGridListModule } from '@angular/material/grid-list';
import { Photo } from 'src/app/classes/photo';
import { ImageCardModule } from '../image-card/image-card.module';

import { ImageGridComponent } from './image-grid.component';

describe('ImageGridComponent', () => {
  let component: ImageGridComponent;
  let fixture: ComponentFixture<ImageGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ImageGridComponent], imports: [MatGridListModule, ImageCardModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create with default values', () => {
    expect(component).toBeTruthy();
    expect(component.col).toBe(3);
    expect(component.photos).toEqual([]);
  });

  describe('col on resize window width', () => {
    it('should be 1 when innerWidth <= 500', () => {
      const spyOnResize = spyOnProperty(window, 'innerWidth').and.returnValue(499);
      window.dispatchEvent(new Event('resize'));

      expect(spyOnResize).toHaveBeenCalled();
      expect(component.col).toBe(1);
    });

    it('should be 2 innerWidth <= 800 && innerWidth > 500', () => {
      const spyOnResize = spyOnProperty(window, 'innerWidth').and.returnValue(600);
      window.dispatchEvent(new Event('resize'));

      expect(spyOnResize).toHaveBeenCalled();
      expect(component.col).toBe(2);
    });

    it('should be 3 innerWidth > 800', () => {
      const spyOnResize = spyOnProperty(window, 'innerWidth').and.returnValue(900);
      window.dispatchEvent(new Event('resize'));

      expect(spyOnResize).toHaveBeenCalled();
      expect(component.col).toBe(3);
    });
  });

  describe('identify', ()=>{
    it('should return an id', ()=>{
      const photo = new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' });
      const result = component.identify(1, photo);

      expect(result).toBe(1);
    })
  })
});
