import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageGridComponent } from './image-grid.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { ImageCardModule } from '../image-card/image-card.module';

@NgModule({
  declarations: [ImageGridComponent],
  imports: [CommonModule, MatGridListModule, ImageCardModule],
  exports: [ImageGridComponent]
})
export class ImageGridModule { }
