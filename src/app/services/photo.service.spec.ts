/* tslint:disable:no-unused-variable */

import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { PhotoService } from './photo.service';

describe('Service: Photo', () => {
  let service: PhotoService;
  let http: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(PhotoService);
    http = TestBed.inject(HttpClient);
    spyOn<any>(service, 'getNewPhotoOnline').and.returnValue('some url');
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  describe('getPhotos', () => {
    it('should add 6 random item to photos', async () => {
      const newPhotos = await service.getPhotos([]);
      const newPhotos2 = await service.getPhotos(newPhotos);

      expect(newPhotos.length).toBe(6);
      expect(newPhotos2.length).toBe(12);
    })
  });

  describe('getNewPhotoOnline', () => {
    it('should not return undefined', () => {
      const res = service['getNewPhotoOnline']();

      expect(res).not.toBeUndefined();
    });

    it('should return a random photo url', async () => {
      const res = await service['getNewPhotoOnline']();

      expect(res).toBe('some url');
    });
  })
});
