import { TestBed } from '@angular/core/testing';

import { FavoriteService } from './favorite.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Overlay } from '@angular/cdk/overlay';
import { Photo } from '../classes/photo';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('FavoriteService', () => {
  let service: FavoriteService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [BrowserAnimationsModule], providers: [MatSnackBar, Overlay] });
    service = TestBed.inject(FavoriteService);
    spyOn(service['_snackBar'], 'open');
  });

  afterAll(()=>{
    localStorage.clear();
    service.favorites.next([]);
  })

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getFavoritesFromLocalStorage', () => {
    it('should return [] if it is empty', () => {
      spyOn(localStorage, 'getItem');
      service.getFavoritesFromLocalStorage();

      expect(service.favorites.getValue()).toEqual([]);
      expect(localStorage.getItem).toHaveBeenCalledTimes(1);
    });

    it('should set favorites when it has values', () => {
      spyOn(localStorage, 'getItem').and.returnValue(JSON.stringify([{ id: 1, name: 'test', description: 'test', src: 'testUrl' }]));
      service.getFavoritesFromLocalStorage();

      expect(service.favorites.getValue()).toEqual([new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' })]);
    });
  })

  describe('setFavoritesInLocalStorage', () => {
    it('should store favorites', () => {
      service.favorites.next([new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' })]);
      service.setFavoritesInLocalStorage();
      const retrievedData = JSON.parse(localStorage.getItem('favorites') as string);

      expect(retrievedData).toEqual([{ id: 1, name: 'test', description: 'test', src: 'testUrl' }]);
    })
  });

  describe('addFavorite', () => {
    it('should show a warning snack if the photo is already added', () => {
      spyOn(service, 'setFavoritesInLocalStorage');
      service.favorites.next([{ id: 1, name: 'test', description: 'test', src: 'testUrl' } as Photo]);
      service.addFavorite({ id: 1, name: 'test', description: 'test', src: 'testUrl' } as Photo);

      expect(service.favorites.getValue()).toEqual([{ id: 1, name: 'test', description: 'test', src: 'testUrl' }]);
      expect(service.setFavoritesInLocalStorage).toHaveBeenCalledTimes(0);
      expect(service['_snackBar'].open).toHaveBeenCalledWith('The photo is already in the favorite list!', undefined, { duration: 3000, panelClass: ['mat-toolbar', 'mat-warn'] });
    });

    it('should add new photo and show successful snack when it is not in the favorites', () => {
      spyOn(service, 'setFavoritesInLocalStorage');
      service.favorites.next([]);
      service.addFavorite({ id: 1, name: 'test', description: 'test', src: 'testUrl' } as Photo);

      expect(service.favorites.getValue()).toEqual([{ id: 1, name: 'test', description: 'test', src: 'testUrl' }]);
      expect(service.setFavoritesInLocalStorage).toHaveBeenCalledTimes(1);
      expect(service['_snackBar'].open).toHaveBeenCalledWith('The photo added to the list successfully!', undefined, { duration: 3000, panelClass: ['mat-toolbar', 'mat-primary'] });
    });
  });

  describe('removeFavorite', () => {
    it('should show a warning snack if the photo is already removed', () => {
      spyOn(service, 'setFavoritesInLocalStorage');
      service.favorites.next([]);
      const result = service.removeFavorite(1);

      expect(service.favorites.getValue()).toEqual([]);
      expect(service.setFavoritesInLocalStorage).toHaveBeenCalledTimes(0);
      expect(service['_snackBar'].open).toHaveBeenCalledWith('The photo is already removed!', undefined, { duration: 3000, panelClass: ['mat-toolbar', 'mat-warn'] });
      expect(result).toBeFalsy();
    });

    it('should remove the photo and show successful snack when it is in the favorites', () => {
      spyOn(service, 'setFavoritesInLocalStorage');
      service.favorites.next([{ id: 1, name: 'test', description: 'test', src: 'testUrl' } as Photo]);
      const result = service.removeFavorite(1);

      expect(service.favorites.getValue()).toEqual([]);
      expect(service.setFavoritesInLocalStorage).toHaveBeenCalledTimes(1);
      expect(service['_snackBar'].open).toHaveBeenCalledWith('The photo is removed successfully from the list!', undefined, { duration: 3000, panelClass: ['mat-toolbar', 'mat-primary'] });
      expect(result).toBeTruthy();
    });
  });

  describe('getFavoriteById', () => {
    it('should show a warning snack if the photo does not exit', () => {
      service.favorites.next([]);
      const favorite = service.getFavoriteById(1);

      expect(favorite).toBeUndefined();
      expect(service['_snackBar'].open).toHaveBeenCalledWith('Your required photo does not exist in the favorites!', undefined, { duration: 3000, panelClass: ['mat-toolbar', 'mat-warn'] });
    });

    it('should return a favorite when exist', () => {
      service.favorites.next([{ id: 1, name: 'test', description: 'test', src: 'testUrl' } as Photo]);

      const favorite = service.getFavoriteById(1);
      expect(favorite).toEqual(new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' }));
      expect(service['_snackBar'].open).toHaveBeenCalledTimes(0);
    });
  });
});
