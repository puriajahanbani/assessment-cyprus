import { Injectable } from '@angular/core';
import { BaseService } from './base-service';
import { Photo } from './../classes/photo';
import { BehaviorSubject } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class FavoriteService extends BaseService {

  public favorites = new BehaviorSubject<Photo[]>([]);
  constructor(private _snackBar: MatSnackBar) {
    super();
    this.getFavoritesFromLocalStorage();
  }

  public getFavoritesFromLocalStorage(): void {
    const favorites: Photo[] = JSON.parse(localStorage.getItem('favorites') || '[]');
    if (favorites) {
      this.favorites.next(favorites.map(fav => new Photo(fav)));
    }
  }

  public setFavoritesInLocalStorage(): void {
    const favorites: { id: number; name: string; description: string; src: string }[] =
      this.favorites.getValue().map(fav => { return { id: fav.id, description: fav.description, name: fav.name, src: fav.src } });
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }

  public addFavorite(favorite: Photo): void {
    const isExist = this.favorites.getValue().find(fav => fav.id === favorite.id);
    if (isExist) {
      this._snackBar.open('The photo is already in the favorite list!', undefined, { duration: 3000, panelClass: ['mat-toolbar', 'mat-warn'] });
    } else {
      this.favorites.next([...this.favorites.getValue(), favorite]);
      this.setFavoritesInLocalStorage();
      this._snackBar.open('The photo added to the list successfully!', undefined, { duration: 3000, panelClass: ['mat-toolbar', 'mat-primary'] });
    }
  }

  public removeFavorite(id: number): boolean {
    const isExist = this.favorites.getValue().find(fav => fav.id === id);
    if (!isExist) {
      this._snackBar.open('The photo is already removed!', undefined, { duration: 3000, panelClass: ['mat-toolbar', 'mat-warn'] });
      return false;
    } else {
      this.favorites.next([...this.favorites.getValue().filter(fav => fav.id !== id)]);
      this.setFavoritesInLocalStorage();
      this._snackBar.open('The photo is removed successfully from the list!', undefined, { duration: 3000, panelClass: ['mat-toolbar', 'mat-primary'] });
      return true;
    }
  }

  public getFavoriteById(id: number): Photo | void {
    const favorite = this.favorites.getValue().find(fav => fav.id === id);
    if (!favorite) {
      this._snackBar.open('Your required photo does not exist in the favorites!', undefined, { duration: 3000, panelClass: ['mat-toolbar', 'mat-warn'] });
    } else {
      return new Photo(favorite);
    }
  }
}
