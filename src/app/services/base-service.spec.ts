import { BaseService } from './base-service';

class Test extends BaseService { }
describe('BaseService', () => {

  it('should create an instance', () => {
    expect(new Test()).toBeTruthy();
  });

  describe('randomInt', () => {
    it('should return a random int between min and max', () => {
      const randNumber = new Test().randomInteger(199, 301);

      expect(randNumber).toBeGreaterThan(200);
      expect(randNumber).toBeLessThan(300);
    })
  })
});
