import { Injectable } from '@angular/core';
import { Photo } from '../classes/photo';
import { BaseService } from './base-service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PhotoService extends BaseService {
  constructor(private http: HttpClient) {
    super();
  }

  public async getPhotos(inputPhotos: Photo[]): Promise<Photo[]> {
    return new Promise(resolve => {
      setTimeout(async () => {
        const photos: Photo[] = [];
        for (let num = 1; num <= 6; num++) {
          const src = await this.getNewPhotoOnline();
          photos.push(new Photo({
            id: num,
            name: 'random name ' + num.toString(),
            description: 'random description ' + num.toString(),
            src,
          }));
        }
        inputPhotos = [...inputPhotos, ...photos];
        resolve(inputPhotos);
      }, this.randomInteger(300, 400));
    })
  }

  private getNewPhotoOnline(): Promise<string> {
    // weird way to get the redirected url to make the photos unique!
    return new Promise(resolve => {
      this.http.get('https://picsum.photos/300/300').toPromise().catch(error => { resolve(error.url); });
    });
  }
}
