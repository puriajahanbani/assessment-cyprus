import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let compiled: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      imports: [MatToolbarModule, MatButtonModule, RouterTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement as HTMLElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should has two buttons and a title', () => {
    const nav = compiled.querySelector('.docs-navbar-header');

    expect(nav?.children.length).toBe(3);
  });

  describe('title', () => {
    it('should be XM test', () => {
      const nav = compiled.querySelector('.docs-navbar-header');

      expect(nav?.children[0].innerHTML).toBe(' XM test ');
    })
  });

  describe('photos-btn', () => {
    it('should has a caption Photos', () => {
      const photosBtn = compiled.querySelector('#photos-btn');

      expect(photosBtn?.innerHTML).toContain('Photos');
    });
  });

  describe('favorites-btn', () => {
    it('should has a caption Favorites', () => {
      const favoritesBtn = compiled.querySelector('#favorites-btn');

      expect(favoritesBtn?.innerHTML).toContain('Favorites');
    });
  });
});
