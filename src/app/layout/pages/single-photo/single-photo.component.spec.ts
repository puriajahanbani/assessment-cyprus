import { Overlay } from '@angular/cdk/overlay';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBar } from '@angular/material/snack-bar';
import { By, Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { Photo } from 'src/app/classes/photo';
import { ImageCardModule } from 'src/app/global/image-card/image-card.module';
import { FavoriteService } from 'src/app/services/favorite.service';

import { SinglePhotoComponent } from './single-photo.component';

describe('SinglePhotoComponent', () => {
  let component: SinglePhotoComponent;
  let fixture: ComponentFixture<SinglePhotoComponent>;
  let routerSpy = { navigate: jasmine.createSpy('navigate') };
  let favoriteService: FavoriteService;
  let title: Title;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SinglePhotoComponent], imports: [RouterTestingModule, MatButtonModule, ImageCardModule, MatIconModule], providers: [FavoriteService, MatSnackBar, Overlay, { provide: Router, useValue: routerSpy }, { provide: ActivatedRoute, useValue: { params: of({ id: '1' }) } }, Title]
    }).compileComponents();

    favoriteService = TestBed.inject(FavoriteService);
    title = TestBed.inject(Title);
    spyOn(title, 'setTitle');
  });

  describe('id is valid', () => {
    beforeEach(() => {
      spyOn(favoriteService, 'getFavoriteById').and.returnValue(new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' }));

      fixture = TestBed.createComponent(SinglePhotoComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should create with title Single-photo', () => {
      expect(component).toBeTruthy();
      expect(title.setTitle).toHaveBeenCalledWith('Single-photo');
    });

    describe('routeSubscription', () => {
      it('should be unsubscribed on ngOnDestroy', () => {
        spyOn(component.routeSubscription, 'unsubscribe');
        component.ngOnDestroy();

        expect(component.routeSubscription.unsubscribe).toHaveBeenCalled();
      });
    })

    it('photo should has value', () => {
      expect(component.photo).toBeInstanceOf(Photo);
      expect(component.photo.id).toBe(1);
      expect(component.photo.name).toBe('test');
      expect(component.photo.description).toBe('test');
      expect(component.photo.src).toBe('testUrl');
    });

    describe('removeFavorite', () => {
      it('should be evoked when user clicks on Remove button', () => {
        spyOn(favoriteService, 'removeFavorite');
        const removeButton = fixture.debugElement.query(By.css('.remove-btn'));
        removeButton.triggerEventHandler('click', 1);

        expect(favoriteService.removeFavorite).toHaveBeenCalledWith(1);
        expect(routerSpy.navigate).toHaveBeenCalledWith(['/favorites']);
      });
    });
  });

  describe('id is not found', () => {
    beforeEach(() => {
      spyOn(favoriteService, 'getFavoriteById').and.returnValue(undefined);
      fixture = TestBed.createComponent(SinglePhotoComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('photo should not has value', () => {
      expect(favoriteService.favorites.getValue()).toEqual([]);
      expect(component.photo).toBeUndefined();
    });

    it('redirect to favorites', () => {
      expect(routerSpy.navigate).toHaveBeenCalledWith(['/favorites']);
    });
  });
});
