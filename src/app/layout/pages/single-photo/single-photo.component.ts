import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Photo } from 'src/app/classes/photo';
import { FavoriteService } from 'src/app/services/favorite.service';

@Component({
  selector: 'app-single-photo',
  templateUrl: './single-photo.component.html',
  styleUrls: ['./single-photo.component.scss']
})
export class SinglePhotoComponent implements OnInit, OnDestroy {
  routeSubscription!: Subscription;
  photo!: Photo;
  constructor(public favoriteService: FavoriteService, private route: ActivatedRoute, private router: Router, private title: Title) {
    this.title.setTitle('Single-photo');
  }

  ngOnInit(): void {
    this.routeSubscription = this.route.params.subscribe((params) => {
      if (params['id']) {
        const id = parseInt(params['id'], 10);
        const photo = this.favoriteService.getFavoriteById(id);
        if (photo) {
          this.photo = photo;
        } else {
          this.router.navigate(['/favorites']);
        }
      };
    })
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
  }

  removeFavorite(id: number) {
    this.favoriteService.removeFavorite(id);
    this.router.navigate(['/favorites']);
  }
}
