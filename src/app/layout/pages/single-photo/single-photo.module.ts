import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SinglePhotoRoutingModule } from './single-photo-routing.module';
import { SinglePhotoComponent } from './single-photo.component';
import { ImageCardModule } from 'src/app/global/image-card/image-card.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [SinglePhotoComponent],
  imports: [CommonModule, SinglePhotoRoutingModule, ImageCardModule, MatButtonModule, MatIconModule]
})
export class SinglePhotoModule { }
