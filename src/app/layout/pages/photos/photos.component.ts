import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { Photo } from 'src/app/classes/photo';
import { FavoriteService } from 'src/app/services/favorite.service';
import { PhotoService } from 'src/app/services/photo.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit {
  page = 1;
  photos: Photo[] = [];
  $photos!: Observable<Photo[]>;
  $isLoading = new BehaviorSubject<boolean>(false);

  @HostListener('window:scroll', ['$event'])
  public scrolled($event: Event) {
    this.onScroll();
  }
  constructor(private photoService: PhotoService, private favoriteService: FavoriteService, private title: Title) {
    this.title.setTitle('Photos');
  }

  ngOnInit(): void {
    this.fetchPhotos();
  }

  async fetchPhotos() {
    this.$isLoading.next(true);
    const photos: Photo[] = await this.photoService.getPhotos(this.photos);
    if (photos) {
      this.$photos = of(photos);
      this.photos = [...photos];
    }
    this.$isLoading.next(false);
  }

  onScroll() {
    if (window.innerHeight + window.scrollY === document.body.scrollHeight) {
      this.fetchPhotos();
    }
  }

  addFavorite(photo: Photo) {
    this.favoriteService.addFavorite(photo);
  }
}
