import { Overlay } from '@angular/cdk/overlay';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBar } from '@angular/material/snack-bar';
import { By, Title } from '@angular/platform-browser';
import { Photo } from 'src/app/classes/photo';
import { ImageGridModule } from 'src/app/global/image-grid/image-grid.module';
import { FavoriteService } from 'src/app/services/favorite.service';
import { PhotoService } from 'src/app/services/photo.service';

import { PhotosComponent } from './photos.component';

describe('PhotosComponent', () => {
  let component: PhotosComponent;
  let fixture: ComponentFixture<PhotosComponent>;
  let photoService: PhotoService;
  let favoriteService: FavoriteService;
  let title: Title;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PhotosComponent], imports: [ImageGridModule, HttpClientTestingModule, MatProgressSpinnerModule], providers: [PhotoService, MatSnackBar, Overlay, Title, FavoriteService]
    }).compileComponents();
  });

  beforeEach(() => {
    photoService = TestBed.inject(PhotoService);
    spyOn(photoService, 'getPhotos').and.returnValue(Promise.resolve([]));

    favoriteService = TestBed.inject(FavoriteService);
    spyOn(favoriteService, 'addFavorite');

    title = TestBed.inject(Title);
    spyOn(title, 'setTitle');

    fixture = TestBed.createComponent(PhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOn(component, 'fetchPhotos');
  });

  it('should create with title Photos', () => {
    expect(component).toBeTruthy();
    expect(title.setTitle).toHaveBeenCalledWith('Photos');
  });

  it('should call fetchPhotos on init', () => {
    component.ngOnInit();

    expect(component.fetchPhotos).toHaveBeenCalledTimes(1);
  });

  it('should isLoading be false after fetching photos ', async () => {
    spyOn(component.$isLoading, 'next')
    await component.fetchPhotos();

    expect(component.$isLoading.next).toHaveBeenCalledWith(false);
  });

  it('should call fetchPhotos on scroll', () => {
    spyOnProperty(window, 'innerHeight').and.returnValue(1000);
    spyOnProperty(window, 'scrollY').and.returnValue(1000);
    spyOnProperty(document.body, 'scrollHeight').and.returnValue(2000);
    window.dispatchEvent(new Event("scroll"));

    expect(component.fetchPhotos).toHaveBeenCalledTimes(1);
  })

  it('should call addFavorite when itemClicked evoked', () => {
    spyOn(component, 'addFavorite');

    const appImageGrid = fixture.debugElement.query(By.css('app-image-grid'));
    appImageGrid.triggerEventHandler('itemClicked', new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' }))

    expect(component.addFavorite).toHaveBeenCalledWith(new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' }));
    expect(component.addFavorite).toHaveBeenCalledTimes(1);
  });

  describe('addFavorite', () => {
    it('should call favoriteService.addFavorite', () => {
      const photo = new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' });
      component.addFavorite(photo);

      expect(favoriteService.addFavorite).toHaveBeenCalledWith(photo);
    })
  })
});
