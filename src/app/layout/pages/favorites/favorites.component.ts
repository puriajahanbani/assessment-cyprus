import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Photo } from 'src/app/classes/photo';
import { FavoriteService } from 'src/app/services/favorite.service';
import { map } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {
  $isEmpty = this.favoriteService.favorites.pipe(map(val => val.length < 1));
  constructor(public favoriteService: FavoriteService, private router: Router, private title: Title) {
    this.title.setTitle('Favorites');
  }

  ngOnInit(): void {
  }

  openSinglePhoto(photo: Photo) {
    if (photo) {
      this.router.navigate(['/photos/' + photo.id])
    }
  }
}
