import { Overlay } from '@angular/cdk/overlay';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { By, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Photo } from 'src/app/classes/photo';
import { ImageGridComponent } from 'src/app/global/image-grid/image-grid.component';
import { ImageGridModule } from 'src/app/global/image-grid/image-grid.module';
import { FavoriteService } from 'src/app/services/favorite.service';

import { FavoritesComponent } from './favorites.component';

describe('FavoritesComponent', () => {
  let component: FavoritesComponent;
  let fixture: ComponentFixture<FavoritesComponent>;
  let routerSpy = { navigate: jasmine.createSpy('navigate') };
  let title: Title;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FavoritesComponent], imports: [ImageGridModule, RouterTestingModule], providers: [FavoriteService, MatSnackBar, Overlay, { provide: Router, useValue: routerSpy }, Title]
    }).compileComponents();
  });

  beforeEach(() => {
    title = TestBed.inject(Title);
    spyOn(title, 'setTitle');

    fixture = TestBed.createComponent(FavoritesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create with title Favorites', () => {
    expect(component).toBeTruthy();
    expect(title.setTitle).toHaveBeenCalledWith('Favorites');
  });

  describe('app-image-grid', () => {
    it('should call openSinglePhoto when itemClicked evoked', () => {
      spyOn(component, 'openSinglePhoto');
      const appImageGrid = fixture.debugElement.query(By.css('app-image-grid'));
      appImageGrid.triggerEventHandler('itemClicked', new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' }))

      expect(component.openSinglePhoto).toHaveBeenCalledWith(new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' }));
    });
  })

  describe('openSinglePhoto', () => {
    it('should call router.navigate', () => {
      component.openSinglePhoto(new Photo({ id: 1, name: 'test', description: 'test', src: 'testUrl' }));

      expect(routerSpy.navigate).toHaveBeenCalledWith(['/photos/1']);
    })
  })
});
